FROM golang:1.13.3-alpine3.10 AS build

ENV CGO_ENABLED 0
WORKDIR /go/src/gitlab.com/vibaiher/green-bot

COPY go.mod go.sum ./
RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=arm go build -a -installsuffix cgo -o main .

FROM alpine:3.10 AS production

RUN apk update \
        && apk upgrade \
        && apk add --no-cache ca-certificates \
        && update-ca-certificates 2>/dev/null || true

WORKDIR /opt

COPY --from=build /go/src/gitlab.com/vibaiher/green-bot/main .

ENTRYPOINT ["/opt/main"]
