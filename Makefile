build:
	docker build --target production -t vibaiher/green-bot:latest .

push: build
	docker push vibaiher/green-bot:latest
